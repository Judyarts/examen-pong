/*******************************************************************************************
*
*   raylib game: FINAL PONG - game template
*
*   developed by [STUDENT NAME HERE]
*
*   This example has been created using raylib 1.0 (www.raylib.com)
*   raylib is licensed under an unmodified zlib/libpng license (View raylib.h for details)
*
*   Copyright (c) 2014 Ramon Santamaria (Ray San)
*
********************************************************************************************/

#include "raylib.h"

typedef enum GameScreen { LOGO, TITLE, STORY, GAMEPLAY, ENDING } GameScreen;

typedef enum Winner { PLAYER, ENEMY, DRAW} Winner;

int main()
{
    // Initialization
    //--------------------------------------------------------------------------------------
    int screenWidth = 800;
    int screenHeight = 450;
    char windowTitle[30] = "raylib game - FINAL PONG";
    
    GameScreen screen = LOGO;
    
    Winner winner = ENEMY;
    
    // TODO: Define required variables here..........................(0.5p)
    // NOTE: Here there are some useful variables (should be initialized)
    Rectangle player;
    player.width = 20;
    player.height = 60;
    player.x = 20;
    player.y = screenHeight/2 - player.height/2;
    int playerSpeedY = 7;
    
    Rectangle enemy;
    enemy.width = 20;
    enemy.height = 60;
    enemy.x = screenWidth - enemy.width - 20;
    enemy.y = screenHeight/2 - enemy.height/2;
    int enemySpeedY = 14;
    
    Vector2 ballPosition;
    ballPosition.x = screenWidth/2;
    ballPosition.y = screenHeight/2;
    Vector2 ballSpeed;
    ballSpeed.x = 10;
    ballSpeed.y = 10;
    int ballRadius = 20;
    
    int playerLife;
    int enemyLife;
    
    Rectangle playerLifeRec;
    playerLifeRec.width = 200;
    playerLifeRec.height = 30;
    playerLifeRec.x = 60;
    playerLifeRec.y = 10;
    
    Rectangle enemyLifeRec;
    enemyLifeRec.width = 200;
    enemyLifeRec.height = 30;
    enemyLifeRec.x = screenWidth - enemyLifeRec.width - 65;
    enemyLifeRec.y = 10;
    
    int secondsCounter = 99;
    
    int framesCounter;          // General pourpose frames counter
    
    int pressStart = 1;
    
    int gameResult = -1;        // 0 - Loose, 1 - Win, -1 - Not defined
    
    float storyPos = 400;
    float storySpeed = 0.5;
    
    bool fadeIn = true;
    
    bool pause = false;
    
    int rotation;
    
    InitWindow(screenWidth, screenHeight, windowTitle);
    
    InitAudioDevice();
    
    // NOTE: If using textures, declare Texture2D variables here (after InitWindow)
    // NOTE: If using SpriteFonts, declare SpriteFont variables here (after InitWindow)
    Texture2D logo = LoadTexture("resources/LOGOpong.png");
    
    Color logoColor = WHITE;
    logoColor.a = 0;
    
    Texture2D title = LoadTexture("resources/TITULO_ULTIMATEPONG.png");
    Texture2D titleBackground = LoadTexture("resources/title_background.png");
    Texture2D storyBackground = LoadTexture("resources/story_background.png");
    Texture2D gameplayBackground = LoadTexture("resources/gameplay_background.png");
    
    Texture2D playerWin = LoadTexture("resources/player_win.png");
    Texture2D playerLose = LoadTexture("resources/player_lose.png");
    Texture2D playerDraw = LoadTexture("resources/player_draw.png");
    
    Texture2D playerTexture = LoadTexture("resources/player.png");
    Texture2D enemyTexture = LoadTexture("resources/enemy.png");
    Texture2D  ballTexture = LoadTexture("resources/ball.png");
    
    Rectangle destRec;
    destRec.x = 0;
    destRec.y = 0;
    destRec.width = ballTexture.width;
    destRec.height = ballTexture.height;
    
    Rectangle sourceRec;
    sourceRec.x = 0;
    sourceRec.y = 0;
    sourceRec.width = ballTexture.width;
    sourceRec.height = ballTexture.height;
    
    
    Vector2 origin;
    origin.x = sourceRec.width/2;
    origin.y = sourceRec.height/2;
    
        
    Vector2 titlePos = {screenWidth/2 - title.width/2, -300};
    
    Sound hit = LoadSound("resources/hit.wav");
    Sound goal = LoadSound("resources/goal_kaka.wav");
    
    Music titleMusic = LoadMusicStream("resources/title.ogg");
    Music music = LoadMusicStream("resources/music.ogg");
    Music victory = LoadMusicStream("resources/victory.ogg");
    Music lose = LoadMusicStream("resources/lose.ogg");
    Music draw = LoadMusicStream("resources/draw.ogg");
    Music storyMusic = LoadMusicStream("resources/story.ogg");
    
    const char story[1024] = " La princesa Aura, tu futura reina, está a punto de ser capturada por Kalos\n principe del planeta Horruculus, el cual se adueña de todo ser poderoso y\n las hace sufrir para alimentarse de su sangre real. Tú como caballero\n del planeta  Harkanu debes ir a rescatar a la princesa Aura.\n \n Para conseguirlo, deberás enfrentarte a Kalos, vencele y conseguirás\n con un poco de suerte el amor de Aura.";

    

    
    // NOTE: If using sound or music, InitAudioDevice() and load Sound variables here (after InitAudioDevice)
    
    SetTargetFPS(60);
    //--------------------------------------------------------------------------------------
    
    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        //----------------------------------------------------------------------------------
        switch(screen) 
        {
            case LOGO: 
            {
                // Update LOGO screen data here!
                
                // TODO: Logo fadeIn and fadeOut logic...............(0.5p)
                if (fadeIn)
                {
                    logoColor.a++;
                    
                    if (logoColor.a == 255)
                        fadeIn = false;
                }
                else
                {
                    framesCounter++;
                    if (framesCounter >= 120)
                    {
                        logoColor.a--;
                        if (logoColor.a == 0)
                        {
                            screen = TITLE;
                            PlayMusicStream(titleMusic);
                            framesCounter = 0;
                        }
                    }
                }

            } break;
            case TITLE: 
            {
                // Update TITLE screen data here!
                UpdateMusicStream(titleMusic);
                
                // TODO: Title animation logic.......................(0.5p)
                
                if (titlePos.y < screenHeight/3 - title.height/2)
                {
                    titlePos.y+= 2;
                }
                else{
                    titlePos.y = screenHeight/3 - title.height/2;
                    framesCounter+=2;
                }

                
                // TODO: "PRESS ENTER" logic.........................(0.5p)
                
                if (framesCounter >= 60)
                {
                    framesCounter = 0;
                    pressStart++;
                }
                
                if (IsKeyPressed(KEY_ENTER))
                {
                    screen = STORY;
                    framesCounter = 0;
                    PlayMusicStream(storyMusic);
                }
                
            } break;
            case STORY:
            {
                UpdateMusicStream(storyMusic);
                storyPos -= storySpeed;
                
                if (storyPos <= -300 || IsKeyPressed(KEY_ENTER))
                {
                    screen = GAMEPLAY;
                    PlayMusicStream(music);
                }
                break;
            }
            case GAMEPLAY:
            { 
                // Update GAMEPLAY screen data here!
                
                
                
                if (!pause)
                {
                    // TODO: Ball movement logic.........................(0.2p)
                    UpdateMusicStream(music); 
                    
                    ballPosition.x += ballSpeed.x;
                    ballPosition.y += ballSpeed.y;
                    
                    destRec.x = ballPosition.x;
                    destRec.y = ballPosition.y;
                    
                    rotation += ballSpeed.x;
                    
                    // TODO: Player movement logic.......................(0.2p)
                    if (IsKeyDown(KEY_UP))
                    {
                        player.y -= playerSpeedY;
                    }
                    else if (IsKeyDown(KEY_DOWN))
                    {
                        player.y += playerSpeedY;
                    }
                    
                    if (player.y <= 55)
                    {
                        player.y = 55;
                    }
                    else if (player.y + player.height >= screenHeight)
                    {
                        player.y = screenHeight - player.height;
                    }
                    
                    // TODO: Enemy movement logic (IA)...................(1p)
                    
                    if (ballPosition.x >= screenWidth/2)
                    {
                        if (ballPosition.y > enemy.y - enemy.height/2)
                        {
                            enemy.y += enemySpeedY;
                        }
                        else if (ballPosition.y < enemy.y - enemy.height/2)
                        {
                            enemy.y -= enemySpeedY;
                        }
                    }
                    
                    if (enemy.y <= 55)
                    {
                        enemy.y = 55;
                    }
                    else if (enemy.y + enemy.height >= screenHeight)
                    {
                        enemy.y = screenHeight - enemy.height;
                    }
                    
                    // TODO: Collision detection (ball-player) logic.....(0.5p)
                    
                    if (CheckCollisionCircleRec(ballPosition, ballRadius, player))
                    {
                        ballSpeed.x *= -1;
                        ballPosition.x = player.x + player.width + ballRadius;
                        PlaySound(hit);
                    }
                    
                    // TODO: Collision detection (ball-enemy) logic......(0.5p)
                    if (CheckCollisionCircleRec(ballPosition, ballRadius, enemy))
                    {
                        ballSpeed.x *= -1;
                        ballPosition.x = enemy.x - ballRadius;
                        PlaySound(hit);
                    }
                    
                    // TODO: Collision detection (ball-limits) logic.....(1p)    
                    if (ballPosition.y + ballRadius >= screenHeight || ballPosition.y - ballRadius <= 55)
                    {
                        ballSpeed.y *= -1;
                        PlaySound(hit);
                    }
                    
                    // TODO: Life bars decrease logic....................(1p)
                    // logica barra de vida
                    if (ballPosition.x + ballRadius >= screenWidth)
                    {
                        ballPosition.x = screenWidth/2;
                        ballPosition.y = screenHeight/2;
                        
                        ballSpeed.x *= -1;
                        
                        enemyLifeRec.width -= 20;
                        enemyLifeRec.x += 20;
                        PlaySound(goal);
                    }
                    else if (ballPosition.x - ballRadius <= 0)
                    {
                        // enemy mete un gol
                        ballPosition.x = screenWidth/2;
                        ballPosition.y = screenHeight/2;
                        
                        ballSpeed.x *= -1;
                        PlaySound(goal);
                        
                        playerLifeRec.width -= 20;
                        
                    }

                    // TODO: Time counter logic..........................(0.2p)
                    
                    framesCounter++;
                    
                    if (framesCounter >= 60)
                    {
                        framesCounter = 0;
                        secondsCounter--;
                    }
                }

                // TODO: Game ending logic...........................(0.2p)
                if (playerLifeRec.width <= 0)
                {
                    winner = ENEMY;
                    screen = ENDING;
                    PlayMusicStream(lose);
                }
                
                if (enemyLifeRec.width <= 0)
                {
                    winner = PLAYER;
                    screen = ENDING;
                    PlayMusicStream(victory);
                }
                
                if (secondsCounter == 0)
                {
                    winner = DRAW;
                    screen = ENDING;
                    PlayMusicStream(draw);
                    
                }
                
                // TODO: Pause button logic..........................(0.2p)
                if (IsKeyPressed(KEY_ENTER))
                {
                    pause = !pause;
                }
                
            } break;
            case ENDING: 
            {
                // Update END screen data here!
                switch(winner)
                {
                    case PLAYER:
                    {
                        UpdateMusicStream(victory);
                        break;
                    }
                    case ENEMY:
                    {
                        UpdateMusicStream(lose);
                        break;
                    }
                    case DRAW:
                    {
                        UpdateMusicStream(draw);
                        break;
                    }
                }
                
                
                // TODO: Replay / Exit game logic....................(0.5p)
                if (IsKeyPressed(KEY_ENTER))
                {
                    screen = GAMEPLAY;
                    enemyLifeRec.width = 200;
                    enemyLifeRec.x = screenWidth - enemyLifeRec.width - 65;
                    playerLifeRec.width = 200;
                    secondsCounter = 99;
                        
                    player.y = screenHeight/2 - player.height/2;
                    enemy.y = screenHeight/2 - enemy.height/2;
                    ballPosition.x = screenWidth/2;
                    ballPosition.y = screenHeight/2;
                    ballSpeed.x = 10;
                    ballSpeed.y = 10;
                    
                    framesCounter = 0;
                }
                
            } break;
            default: break;
        }
        //----------------------------------------------------------------------------------
        
        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();
        
            ClearBackground(RAYWHITE);
            
            switch(screen) 
            {
                case LOGO: 
                {
                    // Draw LOGO screen here!
                    
                    // TODO: Draw Logo...............................(0.2p)
                    
                    DrawTexture(logo, screenWidth/2 - logo.width/2, screenHeight/2 - logo.height/2, logoColor);
                    
                } break;
                case TITLE: 
                {
                    // Draw TITLE screen here!
                    
                    // TODO: Draw Title..............................(0.2p)
                    
                    DrawTexture(titleBackground, 0, 0, WHITE);
                    
                    DrawTexture(title, titlePos.x, titlePos.y, WHITE);
                    
                    if (pressStart % 2 == 0){
                        DrawText ("PRESS START", screenWidth/2 - MeasureText("PRESS START", 50)/2, screenHeight/3 * 2, 50, WHITE);
                    }

                    
                    // TODO: Draw "PRESS ENTER" message..............(0.2p)
                    
                } break;
                case STORY:
                {
                    DrawTexture(storyBackground, 0, 0, WHITE);
                    DrawText(story, 0, storyPos, 20, WHITE);
                    break;
                }
                case GAMEPLAY:
                { 
                    // Draw GAMEPLAY screen here!
                    
                    //DrawText("Esto es el juego", 200, 200, 50, BLACK);
                    DrawTexture(gameplayBackground, 0, 0, WHITE);
                    
                    //DrawCircleV(ballPosition, ballRadius, RED);
                    
                    DrawTexturePro(ballTexture, sourceRec, destRec, origin, rotation, WHITE);
                    
                    // TODO: Draw player and enemy...................(0.2p)
                    
                    //DrawRectangleRec(player, WHITE);
                    
                    DrawTexture(enemyTexture, player.x, player.y, WHITE);
                    
                    DrawTexture(playerTexture, enemy.x, enemy.y, WHITE);
                    
                    //DrawRectangleRec(enemy, BLUE);
                    
                    // TODO: Draw player and enemy life bars.........(0.5p)
                    
                    DrawRectangleRec(playerLifeRec, BLUE);
                    DrawRectangleRec(enemyLifeRec, RED);
                    
                    // TODO: Draw time counter.......................(0.5p)
                    
                    DrawText(FormatText("%02i", secondsCounter), screenWidth/2 - MeasureText("00", 50)/2, 5, 50, WHITE);
                    
                    // TODO: Draw pause message when required........(0.5p)
                    
                    if (pause)
                        DrawText ("PAUSE", screenWidth/2 - MeasureText("PAUSE", 75)/2, screenHeight/3 , 75, WHITE);

                    
                } break;
                case ENDING: 
                {
                    // Draw END screen here!
                    
                    // TODO: Draw ending message (win or loose)......(0.2p)
                    
                    switch(winner){
                        case PLAYER:
                        {
                            DrawTexture(playerWin, 0, 0, WHITE);
                            DrawText ("YOU WIN", screenWidth/2 - MeasureText("YOU WIN", 75)/2, 20 , 75, WHITE);
                        }break;
                        case ENEMY:
                        {
                            DrawTexture(playerLose, 0, 0, WHITE);
                            DrawText ("YOU LOSE", screenWidth/2 - MeasureText("YOU LOSE", 75)/2, 20 , 75, WHITE);
                        }break;
                        case DRAW:
                        {
                            DrawTexture(playerDraw, 0, 0, WHITE);
                            DrawText ("DRAW GAME", screenWidth/2 - MeasureText("DRAW GAME", 75)/2, 20 , 75, WHITE);
                        }break;
                    }
                    
                } break;
                default: break;
            }
        
            DrawFPS(10, 10);
        
        EndDrawing();
        //----------------------------------------------------------------------------------
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------
    
    // NOTE: Unload any Texture2D or SpriteFont loaded here
    
    UnloadTexture(logo);
    UnloadTexture(title);
    UnloadTexture(titleBackground);
    UnloadTexture(storyBackground);
    UnloadTexture(gameplayBackground);
    UnloadTexture(playerTexture);
    UnloadTexture(enemyTexture);
    
    UnloadSound(hit);
    UnloadSound(goal);
        
    UnloadMusicStream(titleMusic);    
    UnloadMusicStream(music);   // Unload music stream buffers from RAM
    UnloadMusicStream(victory);
    UnloadMusicStream(lose);
    UnloadMusicStream(draw);
    UnloadMusicStream(storyMusic);

    CloseAudioDevice();   
    
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------
    
    return 0;
}